﻿using CitationManager.DAL;
using CitationManager.Extensions;
using CitationManager.ImageEditor;
using CitationManager.Model;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace CitationManager
{
    public class PhraseEditorWindowViewModel : BindableBase
    {
        private readonly Entities databaseContext = new Entities();
        private readonly ObservableCollection<Category> availableCategories = new ObservableCollection<Category>();
        private readonly ObservableCollection<Category> assignedCategories = new ObservableCollection<Category>();

        public List<ImageModel> removedImages = new List<ImageModel>();

        private Category[] allCategories;

        private Phrase phrase;
        private IList selectedAvailableCategories;
        private IList selectedAssignedCategories;
        private string title;
        private PhraseEditorWindowContext context;

        public PhraseEditorWindowViewModel()
        {
            ApplyCommand = new DelegateCommand(OnApplyCommand);
            AssignCommand = new DelegateCommand(OnAssignCommand, OnCanAssignCommand);
            UnassignCommand = new DelegateCommand(OnUnassignCommand, OnCanUnassignCommand);
            AddImageCommand = new DelegateCommand(OnAddImageCommand);
            ExportImage100Command = new DelegateCommand<ImageModel>(i => OnExportImageCommand(i.Image100Bytes));
            ExportImage800Command = new DelegateCommand<ImageModel>(i => OnExportImageCommand(i.Image800Bytes));
            RemoveImageCommand = new DelegateCommand<ImageModel>(OnRemoveImageCommand);
            EditImageCommand = new DelegateCommand<ImageModel>(OnEditImageCommand);
            PreviousCommand = new DelegateCommand(OnPreviousCommand);
            NextCommand = new DelegateCommand(OnNextCommand);

            AssignedCategories = CollectionViewSource.GetDefaultView(assignedCategories);
            AssignedCategories.SortDescriptions.Add(new SortDescription(nameof(Category.Name), ListSortDirection.Ascending));

            AvailableCategories = CollectionViewSource.GetDefaultView(availableCategories);
            AvailableCategories.SortDescriptions.Add(new SortDescription(nameof(Category.Name), ListSortDirection.Ascending));
        }

        public DelegateCommand ApplyCommand { get; }

        public DelegateCommand AssignCommand { get; }

        public DelegateCommand UnassignCommand { get; }

        public DelegateCommand AddImageCommand { get; }

        public DelegateCommand<ImageModel> ExportImage100Command { get; }

        public DelegateCommand<ImageModel> ExportImage800Command { get; }

        public DelegateCommand<ImageModel> RemoveImageCommand { get; }

        public DelegateCommand<ImageModel> EditImageCommand { get; }

        public DelegateCommand PreviousCommand { get; }

        public DelegateCommand NextCommand { get; }

        public PhraseEditorWindowContext Context
        {
            get => context;
            set
            {
                context = value;
                RaisePropertyChanged(nameof(Context));
            }
        }

        public string Title
        {
            get => title;
            set
            {
                title = value;
                RaisePropertyChanged(nameof(Title));
            }
        }

        public Phrase Phrase
        {
            get => phrase;
            set
            {
                phrase = value;
                RaisePropertyChanged(nameof(Phrase));
            }
        }

        public ICollectionView AvailableCategories { get; }

        public IList SelectedAvailableCategories
        {
            get => selectedAvailableCategories;
            set
            {
                selectedAvailableCategories = value;
                AssignCommand.RaiseCanExecuteChanged();
            }
        }

        public ICollectionView AssignedCategories { get; }

        public IList SelectedAssignedCategories
        {
            get => selectedAssignedCategories;
            set
            {
                selectedAssignedCategories = value;
                UnassignCommand.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<ImageModel> Images { get; set; } = new ObservableCollection<ImageModel>();

        public void OnInitialized()
        {
            allCategories = databaseContext.Category
                .OrderBy(c => c.Name)
                .ThenBy(c => c.NameRU)
                .ToArray();

            Title = Context.IsCreation ? "Add phrase" : "Edit phrase";
            InitiallizePhrase(Context.PhraseId);
        }

        private bool TryApplyPhrase()
        {
            if (String.IsNullOrWhiteSpace(Phrase.Content) && String.IsNullOrWhiteSpace(Phrase.PhraseRu))
            {
                MessageBox.Show("At least one phrase field is required (Eng or Ru)");
                return false;
            }

            if (Context.IsCreation)
            {
                databaseContext.Phrase.Add(Phrase);
                Phrase.DateTimeAdded = DateTime.Now;
            }

            Phrase.Category.Clear();
            foreach (Category category in assignedCategories)
            {
                Phrase.Category.Add(category);
            }

            foreach (ImageModel image in Images.Where(i => !i.Id.HasValue))
            {
                Phrase.PhrasePicture.Add(new PhrasePicture
                {
                    Phrase = Phrase,
                    PictureData100 = image.Image100Bytes,
                    PictureData800 = image.Image800Bytes,
                });
            }

            foreach (ImageModel image in Images.Where(i => i.Id.HasValue))
            {
                PhrasePicture phrasePicture = Phrase.PhrasePicture.First(p => p.PictureId == image.Id);
                phrasePicture.PictureData100 = image.Image100Bytes;
                phrasePicture.PictureData800 = image.Image800Bytes;
            }

            foreach (ImageModel image in removedImages.Where(i => i.Id.HasValue))
            {
                PhrasePicture phrasePicture = Phrase.PhrasePicture.First(p => p.PictureId == image.Id);
                Phrase.PhrasePicture.Remove(phrasePicture);
                databaseContext.PhrasePicture.Remove(phrasePicture);
            }

            return true;
        }

        private void InitiallizePhrase(int? phraseId)
        {
            removedImages.Clear();
            Images.Clear();
            assignedCategories.Clear();
            availableCategories.Clear();

            if (phraseId.HasValue)
            {
                Phrase = databaseContext.Phrase
                       .Include(nameof(Phrase.Category))
                       .Include(nameof(Phrase.PhrasePicture))
                       .First(p => p.Id == phraseId);

                foreach (PhrasePicture picture in phrase.PhrasePicture)
                {
                    Images.Add(new ImageModel
                    {
                        Id = picture.PictureId,
                        Image100Bytes = picture.PictureData100,
                        Image800Bytes = picture.PictureData800,
                        Image100 = ImageHelper.LoadImage(picture.PictureData100),
                        Image800 = ImageHelper.LoadImage(picture.PictureData800)
                    });
                }
            }
            else
            {
                Phrase = databaseContext.Phrase.Create();
            }

            assignedCategories.AddRange(allCategories.Where(c => Phrase.Category?.Any(pc => pc.Id == c.Id) == true));
            availableCategories.AddRange(allCategories.Where(c => !assignedCategories.Any(ac => ac.Id == c.Id)));
        }

        private void OnApplyCommand()
        {
            if (!TryApplyPhrase())
                return;

            databaseContext.SaveChanges();
            Context.IsConfirmed = true;
            Context.CloseWindow();
        }

        private bool OnCanAssignCommand() => SelectedAvailableCategories?.Count > 0;

        private void OnAssignCommand()
        {
            Category[] categories = SelectedAvailableCategories.OfType<Category>().ToArray();
            assignedCategories.AddRange(categories);

            foreach (Category category in categories)
            {
                availableCategories.Remove(category);
            }
        }

        private bool OnCanUnassignCommand() => SelectedAssignedCategories?.Count > 0;

        private void OnUnassignCommand()
        {
            Category[] categories = selectedAssignedCategories.OfType<Category>().ToArray();
            availableCategories.AddRange(categories);

            foreach (Category category in categories)
            {
                assignedCategories.Remove(category);
            }
        }

        private void OnAddImageCommand()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Image (*.jpg, *.png, *.jpeg, *.gif)|*.jpg;*.png;*.jpeg;*.gif|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() != true)
                return;

            byte[] imageBytes = ImageHelper.LoadImageFromFile(dialog.FileName);

            var context = new ImageEditorWindowContext
            {
                IsImageCreation = true,
                Image800Bytes = imageBytes,
                Image100Bytes = imageBytes
            };

            new ImageEditorWindow(context).ShowDialog();

            if (!context.IsConfirmed)
                return;

            Images.Add(new ImageModel
            {
                Image100Bytes = context.Image100Bytes,
                Image800Bytes = context.Image800Bytes,
                Image100 = ImageHelper.LoadImage(context.Image100Bytes),
                Image800 = ImageHelper.LoadImage(context.Image800Bytes)
            });
        }

        private void OnEditImageCommand(ImageModel image)
        {
            var context = new ImageEditorWindowContext
            {
                Image800Bytes = image.Image800Bytes,
                Image100Bytes = image.Image100Bytes
            };

            new ImageEditorWindow(context).ShowDialog();

            if (!context.IsConfirmed)
                return;

            image.Image100Bytes = context.Image100Bytes;
            image.Image800Bytes = context.Image800Bytes;
            image.Image100 = ImageHelper.LoadImage(context.Image100Bytes);
            image.Image800 = ImageHelper.LoadImage(context.Image800Bytes);
        }

        private void OnRemoveImageCommand(ImageModel image)
        {
            removedImages.Add(image);
            Images.Remove(image);
        }

        private void OnExportImageCommand(byte[] image)
        {
            var dialog = new SaveFileDialog
            {
                Filter = "Image (*.jpg)|*.jpg|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() != true)
                return;

            File.WriteAllBytes(dialog.FileName, image);
        }

        private void OnPreviousCommand()
        {
            int? phraseId = Context.GetPreviousPhraseTo(Phrase.Id);
            if (phrase == null)
            {
                MessageBox.Show("No previous phrases remained");
                return;
            }

            if (!TryApplyPhrase())
                return;

            InitiallizePhrase(phraseId);
        }

        private void OnNextCommand()
        {
            int? phraseId = Context.GetNextPhraseTo(Phrase.Id);
            if (phrase == null)
            {
                MessageBox.Show("No next phrases remained");
                return;
            }

            if (!TryApplyPhrase())
                return;

            InitiallizePhrase(phraseId);
        }
    }
}
