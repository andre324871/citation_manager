﻿using System;

namespace CitationManager.ImageEditor
{
    public class ImageEditorWindowContext
    {
        public bool IsImageCreation { get; set; }

        /// <summary>
        /// Indicates whether dialog is confirmed. False if dialog was cancelled. Output parameter
        /// </summary>
        public bool IsConfirmed { get; set; }

        /// <summary>
        /// Action for view model to close window. Is automatically set by the window
        /// </summary>
        public Action CloseWindow { get; set; }

        /// <summary>
        /// Big frame image. Cannot be null. Output/Input parameter
        /// </summary>
        public byte[] Image800Bytes { get; set; }

        /// <summary>
        /// Small frame image. Cannot be null. Output/Input parameter
        /// </summary>
        public byte[] Image100Bytes { get; set; }
    }
}
