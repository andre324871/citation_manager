﻿using CitationManager.Extensions;
using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
using Prism.Mvvm;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CitationManager.ImageEditor
{
    public class CropContext : BindableBase
    {
        private const int MaxImageWidth = 5000;
        private const int MaxImageHeight = 5000;
        private const int MinImageHeight = 10;
        private const int MinImageWidth = 10;
        private const int MinViewportOverlay = 30;

        private double imageOffsetX;
        private double imageOffsetY;
        private double imageWidth;
        private double imageHeight;
        private BitmapImage image;
        private byte[] originalImageBytes;
        BlackBorder blackBorder;

        public CropContext(double viewportWidth,
            double viewportHeight,
            double frameWidth,
            double frameHeight,
            byte[] imageBytes)
        {
            FrameWidth = frameWidth;
            FrameHeight = frameHeight;
            ViewportWidth = viewportWidth;
            ViewportHeight = viewportHeight;
            SetImage(imageBytes);

            FrameOffsetX = (ViewportWidth - FrameWidth) / 2;
            FrameOffsetY = (ViewportHeight - FrameHeight) / 2;
        }

        public BitmapImage Image
        {
            get => image;
            private set
            {
                image = value;
                RaisePropertyChanged(nameof(Image));
            }
        }

        public byte[] ImageBytes { get; private set; }

        public bool HasBorder => blackBorder != null;

        public double FrameWidth { get; }

        public double FrameHeight { get; }

        public double ViewportWidth { get; }

        public double ViewportHeight { get; }

        public double FrameOffsetX { get; }

        public double FrameOffsetY { get; }

        public double ImageOffsetX
        {
            get => imageOffsetX;
            private set
            {
                imageOffsetX = value;
                RaisePropertyChanged(nameof(ImageOffsetX));
            }
        }

        public double ImageOffsetY
        {
            get => imageOffsetY;
            private set
            {
                imageOffsetY = value;
                RaisePropertyChanged(nameof(ImageOffsetY));
            }
        }

        public double ImageWidth
        {
            get => imageWidth;
            private set
            {
                imageWidth = value;
                RaisePropertyChanged(nameof(ImageWidth));
            }
        }

        public double ImageHeight
        {
            get => imageHeight;
            private set
            {
                imageHeight = value;
                RaisePropertyChanged(nameof(ImageHeight));
            }
        }

        public void Zoom(double coefficient)
        {
            double newImageHeight = ImageHeight * coefficient;
            double newImageWidth = ImageWidth * coefficient;

            if (newImageHeight > MaxImageHeight)
                return;

            if (newImageWidth > MaxImageWidth)
                return;

            if (newImageHeight < MinImageHeight)
                return;

            if (newImageWidth < MinImageWidth)
                return;

            if (blackBorder != null)
            {
                blackBorder.BorderThickness *= coefficient;
                blackBorder.Height *= coefficient;
                blackBorder.Width *= coefficient;
                blackBorder.OffsetX *= coefficient;
                blackBorder.OffsetY *= coefficient;
            }

            ImageHeight = newImageHeight;
            ImageWidth = newImageWidth;

            Update();
            CenterImage();
        }

        public void Move(double deltaX, double deltaY) => MoveTo(imageOffsetX + deltaX, imageOffsetY + deltaY);

        public void MoveTo(double offsetX, double offsetY)
        {
            if (ViewportWidth - offsetX < MinViewportOverlay)
                return;

            if (ViewportHeight - offsetY < MinViewportOverlay)
                return;

            if (offsetX + imageWidth < MinViewportOverlay)
                return;

            if (offsetY + imageHeight < MinViewportOverlay)
                return;

            ImageOffsetX = offsetX;
            ImageOffsetY = offsetY;
        }

        public void CenterImage()
        {
            ImageOffsetX = (ViewportWidth - ImageWidth) / 2;
            ImageOffsetY = (ViewportHeight - ImageHeight) / 2;
        }

        public void Fit(bool fitWholeImage = false)
        {
            if (fitWholeImage)
            {
                Zoom(Math.Min(FrameWidth / ImageWidth, FrameHeight / ImageHeight));
                CenterImage();
            }
            else
            {
                Zoom(FrameWidth / ImageWidth);
                ImageOffsetX = FrameOffsetX;
                ImageOffsetY = FrameOffsetY;
            }
        }

        public void SetImage(byte[] imageBytes)
        {
            originalImageBytes = imageBytes;
            image = ImageHelper.LoadImage(imageBytes);
            ImageWidth = image.PixelWidth;
            ImageHeight = image.PixelHeight;

            blackBorder = null;
            RaisePropertyChanged(nameof(HasBorder));

            Update();
        }

        public void CopyImageTo(CropContext cropContext)
        {
            cropContext.SetImage(originalImageBytes);
            cropContext.CenterImage();
        }

        public void ToggleBorder(int borderThicknessInPixels)
        {
            if (HasBorder)
            {
                blackBorder = null;
            } 
            else
            {
                CropBorderParameters cropBorder = GetCropBorder();

                blackBorder = new BlackBorder
                {
                    BorderThickness = borderThicknessInPixels,
                    Height = cropBorder.Height,
                    Width = cropBorder.Width,
                    OffsetX = cropBorder.OffsetX,
                    OffsetY = cropBorder.OffsetY
                };
            }

            Update();
            RaisePropertyChanged(nameof(HasBorder));
        }

        public byte[] GetCroppedImageBytes()
        {
            CropBorderParameters cropBorder = GetCropBorder();

            using (var inStream = new MemoryStream(ImageBytes))
            using (var outStream = new MemoryStream())
            using (var imageFactory = new ImageFactory(preserveExifData: true))
            {
                var image1 = ImageHelper.LoadImage(ImageBytes);

                imageFactory.Load(inStream)
                            .Crop(new System.Drawing.Rectangle((int)cropBorder.OffsetX, (int)cropBorder.OffsetY, (int)cropBorder.Width, (int)cropBorder.Height))
                            .Resize(new System.Drawing.Size((int)cropBorder.Width, (int)cropBorder.Height))
                            .Quality(100)
                            .Format(new JpegFormat { Quality = 100 })
                            .BackgroundColor(System.Drawing.Color.White)
                            .Save(outStream);

                outStream.Position = 0;

                var bytes = outStream.GetBuffer();
                var image2 = ImageHelper.LoadImage(bytes);
                return bytes;
            }
        }

        private CropBorderParameters GetCropBorder()
        {
            double frameWidth = FrameWidth;
            double frameHeight = FrameHeight;
            double frameOffsetX = FrameOffsetX;
            double frameOffsetY = FrameOffsetY;

            if (ImageOffsetX > FrameOffsetX)
            {
                double diff = ImageOffsetX - FrameOffsetX;
                frameWidth -= diff;
                frameOffsetX += diff;
            }

            if (ImageOffsetY > FrameOffsetY)
            {
                double diff = ImageOffsetY - FrameOffsetY;
                frameHeight -= diff;
                frameOffsetY += diff;
            }

            double maxImageX = ImageOffsetX + ImageWidth;
            double maxImageY = ImageOffsetY + ImageHeight;

            double maxFrameX = FrameOffsetX + FrameWidth;
            double maxFrameY = FrameOffsetY + FrameHeight;

            if (maxFrameX > maxImageX)
            {
                double diff = maxFrameX - maxImageX;
                frameWidth -= diff;
            }

            if (maxFrameY > maxImageY)
            {
                double diff = maxFrameY - maxImageY;
                frameHeight -= diff;
            }

            double zoom = Image.PixelWidth / ImageWidth;

            return new CropBorderParameters
            {
                Width = frameWidth,
                Height = frameHeight,
                OffsetX = frameOffsetX - ImageOffsetX,
                OffsetY = frameOffsetY - ImageOffsetY
            };
        }

        private void Update()
        {
            byte[] imageBytes = ImageHelper.Resize(originalImageBytes, (int)ImageWidth, (int)ImageHeight);

            if (blackBorder != null)
            {
                imageBytes = DrawBlackBorder(imageBytes);
            }

            ImageBytes = imageBytes;
            Image = ImageHelper.LoadImage(imageBytes);
        }

        public byte[] DrawBlackBorder(byte[] image)
        {
            var border = new Border
            {
                Width = blackBorder.Width,
                Height = blackBorder.Height,
                BorderBrush = Brushes.Black,
                BorderThickness = new Thickness(blackBorder.BorderThickness),
                SnapsToDevicePixels = true
            };

            border.Measure(new Size(blackBorder.Width, blackBorder.Height));
            border.Arrange(new Rect(new Size(blackBorder.Width, blackBorder.Height)));

            var target = new RenderTargetBitmap((int)Math.Round(border.RenderSize.Width), (int)Math.Round(border.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
            target.Render(border);

            var encoder = new PngBitmapEncoder();
            var outputFrame = BitmapFrame.Create(target);
            encoder.Frames.Add(outputFrame);

            using (var borderStream = new MemoryStream())
            using (var inStream = new MemoryStream(image))
            using (var outStream = new MemoryStream())
            using (var imageFactory = new ImageFactory(preserveExifData: true))
            {
                encoder.Save(borderStream);
                borderStream.Position = 0;

                imageFactory.Load(inStream)
                            .Overlay(new ImageLayer
                            {
                                Image = System.Drawing.Image.FromStream(borderStream),
                                Position = new System.Drawing.Point((int)blackBorder.OffsetX, (int)blackBorder.OffsetY),
                                Size = new System.Drawing.Size((int)blackBorder.Width, (int)blackBorder.Height)
                            })
                            .Quality(100)
                            .Format(new JpegFormat { Quality = 100 })
                            .BackgroundColor(System.Drawing.Color.White)
                            .Save(outStream);

                outStream.Position = 0;

                return outStream.GetBuffer();
            }
        }
    }

    class CropBorderParameters
    {
        public double Width { get; set; }

        public double Height { get; set; }

        public double OffsetX { get; set; }

        public double OffsetY { get; set; }
    }

    class BlackBorder : CropBorderParameters
    {
        public double BorderThickness { get; set; }
    }
}
