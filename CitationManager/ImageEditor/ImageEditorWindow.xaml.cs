﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CitationManager.ImageEditor
{
    public partial class ImageEditorWindow
    {
        private readonly ImageEditorWindowContext context;
        private ImageEditorWindowViewModel viewModel;
        private Image draggedImage;
        private Point mousePosition;

        public ImageEditorWindow(ImageEditorWindowContext context)
        {
            InitializeComponent();
            this.context = context;
            PreviewKeyDown += OnPreviewKeyDown;
        }

        private void OnCanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var image = e.Source as Image;

            if (image != null && Canvas.CaptureMouse())
            {
                mousePosition = e.GetPosition(Canvas);
                draggedImage = image;
            }
        }

        private void OnCanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (draggedImage != null)
            {
                Canvas.ReleaseMouseCapture();
                draggedImage = null;
            }
        }

        private void OnCanvasMouseMove(object sender, MouseEventArgs e)
        {
            if (draggedImage != null)
            {
                var position = e.GetPosition(Canvas);
                var offset = position - mousePosition;
                mousePosition = position;
                viewModel.CurrentCropContext.Move(offset.X, offset.Y);
            }
        }

        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            viewModel = (ImageEditorWindowViewModel)DataContext;
            context.CloseWindow = Close;
            viewModel.Context = context;
            viewModel.OnInitialized(Canvas.ActualWidth, Canvas.ActualHeight);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (new[] { Key.OemPlus, Key.Add }.Contains(e.Key))
                viewModel.CurrentCropContext.Zoom(1 + (viewModel.Settings.ZoomStepInPercents / 100.0));

            if (new[] { Key.OemMinus, Key.Subtract }.Contains(e.Key))
                viewModel.CurrentCropContext.Zoom(1 - (viewModel.Settings.ZoomStepInPercents / 100.0));

            int moveStepInPixels = 10;
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                moveStepInPixels = 1;

            if (e.Key == Key.Left)
                viewModel.CurrentCropContext.Move(-moveStepInPixels, 0);

            if (e.Key == Key.Right)
                viewModel.CurrentCropContext.Move(moveStepInPixels, 0);

            if (e.Key == Key.Up)
                viewModel.CurrentCropContext.Move(0, -moveStepInPixels);

            if (e.Key == Key.Down)
                viewModel.CurrentCropContext.Move(0, +moveStepInPixels);
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Escape)
                return;

            Close();
            e.Handled = true;
        }
    }
}
