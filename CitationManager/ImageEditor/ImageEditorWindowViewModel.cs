﻿using CitationManager.Extensions;
using CitationManager.Model;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;

namespace CitationManager.ImageEditor
{
    public class ImageEditorWindowViewModel : BindableBase
    {
        private string title;
        private CropContext currentCropContext;
        private CropContext bigCropContext;
        private CropContext smallCropContext;

        public ImageEditorWindowViewModel()
        {
            ApplyCommand = new DelegateCommand(OnApplyCommand);
            SelectSmallFrameCommand = new DelegateCommand(() => OnSelectFrameCommand(isBigFrame: false), () => OnCanSelectFrameCommand(isBigFrame: false));
            SelectBigFrameCommand = new DelegateCommand(() => OnSelectFrameCommand(isBigFrame: true), () => OnCanSelectFrameCommand(isBigFrame: true));
            FitCommand = new DelegateCommand(OnFitCommand);
            CenterCommand = new DelegateCommand(OnCenterCommand);
            ChooseFileCommand = new DelegateCommand(OnChooseFileCommand);
            ToggleBorderCommand = new DelegateCommand(OnToggleBorderCommand);
            CopyToSmallFrameCommand = new DelegateCommand(OnCopyToSmallFrameCommand);
            Settings = Settings.Current;
        }

        public DelegateCommand ApplyCommand { get; }

        public DelegateCommand SelectSmallFrameCommand { get; }

        public DelegateCommand SelectBigFrameCommand { get; }

        public DelegateCommand FitCommand { get; }

        public DelegateCommand CenterCommand { get; }

        public DelegateCommand ChooseFileCommand { get; }

        public DelegateCommand ToggleBorderCommand { get; }

        public DelegateCommand CopyToSmallFrameCommand { get; }

        public ImageEditorWindowContext Context { get; set; }

        public string Title
        {
            get => title;
            set
            {
                title = value;
                RaisePropertyChanged(nameof(Title));
            }
        }

        public CropContext CurrentCropContext
        {
            get => currentCropContext;
            set
            {
                currentCropContext = value;
                RaisePropertyChanged(nameof(CurrentCropContext));
                RaisePropertyChanged(nameof(IsSmallFrameSelected));
                RaisePropertyChanged(nameof(CopyToSmallFrameButtonText));
                SelectSmallFrameCommand.RaiseCanExecuteChanged();
                SelectBigFrameCommand.RaiseCanExecuteChanged();
            }
        }


        /// <summary>
        /// Determited whether editing small frame. In false then editing big frame
        /// </summary>
        public bool IsSmallFrameSelected => CurrentCropContext == smallCropContext;

        public Settings Settings { get; }

        public string CopyToSmallFrameButtonText => IsSmallFrameSelected 
            ? $"Copy from {Settings.BigFrameString}"
            : $"Copy to {Settings.SmallFrameString}";

        public string BigFrameSize { get; set; }

        public string SmallFrameSize { get; set; }

        public void OnInitialized(double canvasWidth, double canvasHeight)
        {
            Title = "Frame editor";

            smallCropContext = new CropContext(canvasWidth, canvasHeight, Settings.SmallFrameWidth, Settings.SmallFrameHeight, Context.Image100Bytes);
            bigCropContext = new CropContext(canvasWidth, canvasHeight, Settings.BigFrameWidth, Settings.BigFrameHeight, Context.Image800Bytes);

            if (Context.IsImageCreation)
            {
                smallCropContext.Fit(fitWholeImage: true);
                bigCropContext.Fit();
            }
            else
            {
                smallCropContext.CenterImage();
                bigCropContext.CenterImage();
            }
           
            OnSelectFrameCommand(isBigFrame: true);
        }

        private void OnApplyCommand()
        {
            Context.Image100Bytes = smallCropContext.GetCroppedImageBytes();
            Context.Image800Bytes = bigCropContext.GetCroppedImageBytes();
            Context.IsConfirmed = true;
            Context.CloseWindow();
        }

        private bool OnCanSelectFrameCommand(bool isBigFrame)
        {
            if (isBigFrame && IsSmallFrameSelected)
                return true;

            if (!isBigFrame && !IsSmallFrameSelected)
                return true;

            return false;
        }

        private void OnSelectFrameCommand(bool isBigFrame)
        {
            CurrentCropContext = isBigFrame ? bigCropContext : smallCropContext;
        }

        private void OnFitCommand() => CurrentCropContext.Fit();

        private void OnCenterCommand() => CurrentCropContext.CenterImage();

        private void OnChooseFileCommand()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Image (*.jpg, *.png, *.jpeg, *.gif)|*.jpg;*.png;*.jpeg;*.gif|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() != true)
                return;

            CurrentCropContext.SetImage(ImageHelper.LoadImageFromFile(dialog.FileName));
            CurrentCropContext.CenterImage();
        }

        private void OnToggleBorderCommand() 
            => CurrentCropContext.ToggleBorder(IsSmallFrameSelected ? Settings.SmallFrameBorderWidthInPixels : Settings.BigFrameBorderWidthInPixels);

        private void OnCopyToSmallFrameCommand()
        {
            bigCropContext.CopyImageTo(smallCropContext);
            smallCropContext.Fit(fitWholeImage: true);

            if (!IsSmallFrameSelected)
            {
                OnSelectFrameCommand(isBigFrame: false);
            }
        }
    }
}
