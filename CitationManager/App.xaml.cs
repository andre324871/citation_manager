﻿using System.Windows;
using System.Windows.Threading;

namespace CitationManager
{
    public partial class App : Application
    {
        public App()
        {
            Dispatcher.UnhandledException += OnDispatcherUnhandledException;
        }

        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show($"Exception happened: {e.Exception.ToString()}", "Exception");
            e.Handled = true;
        }
    }
}
