﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Category](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[NameRU] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Category] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [dbo].[Phrase](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Phrase] [nvarchar](max) NULL,
	[PhraseRu] [nvarchar](max) NULL,
	[DateTimeAdded] [datetime] NOT NULL
 CONSTRAINT [PK_dbo.Phrase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE [dbo].[PhraseCategory](
	[PhraseId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.PhraseCategory] PRIMARY KEY CLUSTERED 
(
	[PhraseId] ASC,
	[CategoryId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PhraseCategory]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PhraseCategory_dbo.Category_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PhraseCategory] CHECK CONSTRAINT [FK_dbo.PhraseCategory_dbo.Category_CategoryId]
GO

ALTER TABLE [dbo].[PhraseCategory]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PhraseCategory_dbo.Phrase_PhraseId] FOREIGN KEY([PhraseId])
REFERENCES [dbo].[Phrase] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PhraseCategory] CHECK CONSTRAINT [FK_dbo.PhraseCategory_dbo.Phrase_PhraseId]
GO


CREATE TABLE [dbo].[PhrasePicture](
	[PictureId] [int] IDENTITY(1,1) NOT NULL,
	[PhraseId] [int] NOT NULL,
	[PictureData100] [varbinary](max) NULL,
	[PictureData800] [varbinary](max) NULL,
 CONSTRAINT [PK_dbo.PhrasePicture] PRIMARY KEY CLUSTERED 
(
	[PictureId] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[PhrasePicture]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PhrasePicture_dbo.Phrase_PhraseId] FOREIGN KEY([PhraseId])
REFERENCES [dbo].[Phrase] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PhrasePicture] CHECK CONSTRAINT [FK_dbo.PhrasePicture_dbo.Phrase_PhraseId]
GO


SET IDENTITY_INSERT [dbo].[Category] ON

INSERT INTO [dbo].[Category]
           ([Id], [Name], [NameRU]) VALUES
	(1, N'Chakras'					 ,N'Чакры'),
	(2, N'Common behavior'			 ,N'Совместное поведение'),
	(3, N'Yoga'						 ,N'Йога'),
	(4, N'Health'					 ,N'Здоровье'),
	(5, N'Chi'						 ,N'Чи'),
	(6, N'Evolution'				 ,N'Эволюция'),
	(7, N'Stars'					 ,N'Звёзды'),
	(8, N'Future'					 ,N'Будущее'),
	(9, N'Religion'					 ,N'Религия'),
	(10, N'Karma'					 ,N'Карма'),
	(11, N'Sin'						 ,N'Грех'),
	(12, N'Higher intelligence'		 ,N'Высший разум'),
	(13, N'Holy books'				 ,N'Священные книги'),
	(14, N'Laws and rules'			 ,N'Законы и правила'),
	(15, N'Satire'					 ,N'Сатира'),
	(16, N'Climate change'			 ,N'Изменение климата'),
	(17, N'Life on our planet'		 ,N'Жизнь на нашей планете'),
	(18, N'Energy'					 ,N'Энергия'),
	(19, N'Resource consumption'	 ,N'Потребление ресурсов'),
	(20, N'Miracles'				 ,N'Чудеса'),
	(21, N'Magic'					 ,N'Магия'),
	(22, N'About me'				 ,N'Обо мне'),
	(23, N'About us'				 ,N'О нас'),
	(24, N'Meaning of war'			 ,N'Смысл войны'),
	(25, N'Tragedy'					 ,N'Трагедия'),
	(26, N'Sacred'					 ,N'Святое'),
	(27, N'Commonality of languages' ,N'Общность языков'),
	(28, N'Poetry'					 ,N'Поэзия'),
	(29, N'Psychedelics'			 ,N'Психоделики'),
	(30, N'Hallucinations'			 ,N'Галлюцинации'),
	(31, N'Feelings and sensations'	 ,N'Чувства и ощущения'),
	(32, N'God’s plan'				 ,N'План Бога о нашем существовании'),
	(33, N'Science'					 ,N'Наука'),
	(34, N'Technology'				 ,N'Технология'),
	(35, N'Nature'					 ,N'Природа'),
	(36, N'Internet'				 ,N'Интернет')

SET IDENTITY_INSERT [dbo].[Category] OFF
GO

ALTER TABLE [dbo].[Phrase] ADD [Importance] [smallint] NOT NULL DEFAULT(0)
GO