﻿using CitationManager.Properties;
using System;
using System.Data.Entity;

namespace CitationManager.DAL
{
    public class DatabaseInitializer : IDatabaseInitializer<Entities>
    {
        public void InitializeDatabase(Entities context)
        {
            if (!context.Database.Exists())
            {
                context.Database.Create();

                foreach (var sqlStatement in Resources.CreateDatabase.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    context.Database.ExecuteSqlCommand(sqlStatement);
                }
            }
        }
    }
}
