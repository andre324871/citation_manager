﻿using System.Windows;
using System.Windows.Input;

namespace CitationManager
{
    public partial class MainWindow
    {
        private MainWindowViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            viewModel = (MainWindowViewModel)DataContext;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            viewModel.OnLoaded();
        }

        private void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (viewModel.EditCommand.CanExecute())
            {
                viewModel.EditCommand.Execute();
            }
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Delete)
                return;

            viewModel.DeleteCommand.Execute();
        }

        private void OnSearchKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                viewModel.SearchCommand.Execute();

            if (e.Key != Key.Delete)
                return;

            // Stop propagation. Avoid pharases detete command when focus is on search textbox. User uses delete here to remove characters in textbox.
            e.Handled = true;
        }
    }
}
