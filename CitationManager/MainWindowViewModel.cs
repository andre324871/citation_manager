﻿using CitationManager.DAL;
using CitationManager.Extensions;
using CitationManager.Model;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows;

namespace CitationManager
{
    public class MainWindowViewModel : BindableBase
    {
        private const int PageSize = 50;
        private const int MaxCategoriesToShow = 3;

        private Entities databaseContext;

        /// <summary>
        /// Zero-based index of current page
        /// </summary>
        private int currentPage = 0;

        private int pageCount = 0;

        private string pageText;

        private string pageTextToolTip;

        private bool isUpdating;

        private PhraseModel[] phrases;
        private Category[] categories;
        private Category selectedCategory;
        private string searchText;
        private IList selectedPhrases;
        private IQueryable<Phrase> phrasesQuery;

        public MainWindowViewModel()
        {
            AddCommand = new DelegateCommand(OnAddCommand);
            EditCommand = new DelegateCommand(OnEditCommand, OnCanEditCommand);
            DeleteCommand = new DelegateCommand(OnDeleteCommand, OnCanDeleteCommand);
            SwitchPageCommand = new DelegateCommand<PageNavigation>(OnSwitchPageCommand, OnCanSwitchPageCommand);
            RecreateDatabaseCommand = new DelegateCommand(OnRecreateDatabaseCommand);
            CategoriesCommand = new DelegateCommand(OnCategoriesCommand);
            ImportCommand = new DelegateCommand(OnImportCommand);
            SearchCommand = new DelegateCommand(OnSearchCommand);
        }

        public void OnLoaded()
        {
            Update();
        }

        public DelegateCommand AddCommand { get; }

        public DelegateCommand EditCommand { get; }

        public DelegateCommand DeleteCommand { get; }

        public DelegateCommand RecreateDatabaseCommand { get; }

        public DelegateCommand CategoriesCommand { get; }

        public DelegateCommand ImportCommand { get; }

        public DelegateCommand<PageNavigation> SwitchPageCommand { get; }

        public DelegateCommand SearchCommand { get; }

        public string PageText
        {
            get => pageText;
            set
            {
                pageText = value;
                RaisePropertyChanged(nameof(PageText));
            }
        }

        public string PageTextToolTip
        {
            get => pageTextToolTip;
            set
            {
                pageTextToolTip = value;
                RaisePropertyChanged(nameof(PageTextToolTip));
            }
        }

        public PhraseModel[] Phrases
        {
            get => phrases;
            set
            {
                phrases = value;
                RaisePropertyChanged(nameof(Phrases));
            }
        }

        public IList SelectedPhrases
        {
            get => selectedPhrases;
            set
            {
                selectedPhrases = value;
                EditCommand.RaiseCanExecuteChanged();
                DeleteCommand.RaiseCanExecuteChanged();
            }
        }

        public Category[] Categories
        {
            get => categories;
            set
            {
                categories = value;
                RaisePropertyChanged(nameof(Categories));
            }
        }

        public Category SelectedCategory
        {
            get => selectedCategory;
            set
            {
                selectedCategory = value;
                Update();
            }
        }

        public string SearchText
        {
            get => searchText;
            set
            {
                searchText = value;
                RaisePropertyChanged(nameof(SearchText));
            }
        }

        private void Update()
        {
            try
            {
                if (isUpdating)
                    return;

                isUpdating = true;

                // To show fresh results 
                databaseContext?.Dispose();
                databaseContext = new Entities();

                int? oldSelectedCategoryId = selectedCategory?.Id;
                var defaultCategory = new Category { Id = 0, Name = "-- Filter by category --" };
                Categories = new[] { defaultCategory }.Union(databaseContext.Category).ToArray();
                selectedCategory = Categories.FirstOrDefault(c => c.Id == oldSelectedCategoryId) ?? defaultCategory;
                RaisePropertyChanged(nameof(SelectedCategory));

                phrasesQuery = databaseContext.Phrase;
                IQueryable<Phrase> totalQuery = databaseContext.Phrase;
                if (!string.IsNullOrWhiteSpace(SearchText))
                {
                    phrasesQuery = phrasesQuery.Where(p => p.Content.Contains(SearchText) || p.PhraseRu.Contains(SearchText));
                }

                if (selectedCategory.Id != 0)
                {
                    totalQuery = totalQuery.Where(p => p.Category.Any(c => c.Id == selectedCategory.Id));
                    phrasesQuery = phrasesQuery.Where(p => p.Category.Any(c => c.Id == selectedCategory.Id));
                }

                int phrasesCount = phrasesQuery.Count();

                if (phrasesCount == 0)
                {
                    pageCount = 1;
                }
                else
                {
                    pageCount = phrasesCount / PageSize + (phrasesCount % PageSize > 0 ? 1 : 0);
                }

                if (currentPage < 0)
                {
                    currentPage = pageCount - 1;
                }

                if (currentPage >= pageCount)
                {
                    currentPage = 0;
                }

                PageText = $"Page {currentPage + 1} of {pageCount} (Total {totalQuery.Count()} items)";
                PageTextToolTip = $"Showing {PageSize} items per page";

                phrasesQuery = phrasesQuery
                    .Include(nameof(Phrase.Category)) // Avoid a lot of additional queries
                    .OrderByDescending(p => p.DateTimeAdded)
                    .ThenBy(p => p.Id);

                Phrases = phrasesQuery
                    .Skip(PageSize * currentPage)
                    .Take(PageSize)
                    .Select(p => new
                    {
                        Phrase = p,
                        p.Category, // To make include to work
                        ContainsImage = p.PhrasePicture.Any()
                    })
                    .ToArray() // Execute query here
                    .OrderByDescending(i => i.Phrase.DateTimeAdded)
                    .ThenBy(i => i.Phrase.Content)
                    .ThenBy(i => i.Phrase.PhraseRu)
                    .Select(i =>
                    {
                        string[] categoryNames = i.Category.OrderBy(c => c.Name).Select(c => c.Name).ToArray();
                        string categoriesString;
                        if (categoryNames.Length > MaxCategoriesToShow)
                        {
                            categoriesString = $"{string.Join(", ", categoryNames.Take(MaxCategoriesToShow))} and {categoryNames.Length - MaxCategoriesToShow} more";
                        }
                        else
                        {
                            categoriesString = string.Join(", ", categoryNames);
                        }

                        return new PhraseModel
                        {
                            Id = i.Phrase.Id,
                            Content = i.Phrase.Content,
                            ContentRU = i.Phrase.PhraseRu,
                            Categories = categoriesString,
                            DateTimeAdded = i.Phrase.DateTimeAdded,
                            ContainsImage = i.ContainsImage,
                            IsImportant = i.Phrase.Importance == 1
                        };
                    })
                    .ToArray();

                SelectedPhrases?.Clear();
                SwitchPageCommand.RaiseCanExecuteChanged();
            } finally
            {
                isUpdating = false;
            }
        }

        private void OnAddCommand()
        {
            var context = new PhraseEditorWindowContext();
            new PhraseEditorWindow(context).ShowDialog();

            if (context.IsConfirmed)
            {
                Update();
            }
        }

        private bool OnCanEditCommand() => SelectedPhrases?.Count == 1;

        private void OnEditCommand()
        {
            var context = new PhraseEditorWindowContext
            {
                PhraseId = ((PhraseModel)SelectedPhrases[0]).Id,
                GetNextPhraseTo = (int phraseId) =>
                {
                    PhraseModel nextPhrase = Phrases.SkipWhile(p => p.Id != phraseId).Skip(1).Take(1).FirstOrDefault();
                    if (nextPhrase == null)
                    {
                        SwitchPageCommand.Execute(new PageNavigation(PageNavigationMode.Next));
                        nextPhrase = Phrases.FirstOrDefault();
                    }

                    return nextPhrase?.Id;
                },
                GetPreviousPhraseTo = (int phraseId) =>
                {
                    int currentPhraseIndex = Array.IndexOf(Phrases, Phrases.First(p => p.Id == phraseId));
                    if (currentPhraseIndex == 0)
                    {
                        SwitchPageCommand.Execute(new PageNavigation(PageNavigationMode.Previous));
                        return Phrases.LastOrDefault()?.Id;
                    }
                    else
                    {
                        return Phrases[currentPhraseIndex - 1].Id;
                    }
                }
            };

            new PhraseEditorWindow(context).ShowDialog();

            if (context.IsConfirmed)
            {
                Update();
            }
        }

        private bool OnCanDeleteCommand() => SelectedPhrases?.Count > 0;

        private void OnDeleteCommand()
        {
            if (!OnCanDeleteCommand())
                return;

            string message;
            if (SelectedPhrases.Count == 1)
            {
                var phrase = ((PhraseModel)SelectedPhrases[0]).Content;
                if (string.IsNullOrWhiteSpace(phrase))
                    phrase = ((PhraseModel)SelectedPhrases[0]).ContentRU;

                message = $"Are you sure to remove phrase '{phrase}'?";
            }
            else
            {
                message = $"Are you sure to remove {SelectedPhrases.Count} phrases?";
            }

            if (MessageBox.Show(message, "Confirm removal", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            foreach (PhraseModel phrase in SelectedPhrases)
            {
                databaseContext.Phrase.Remove(databaseContext.Phrase.First(p => p.Id == phrase.Id));
            }

            databaseContext.SaveChanges();
            Update();
        }

        private bool OnCanSwitchPageCommand(PageNavigation pageNavigation)
        {
            switch (pageNavigation.Mode)
            {
                case PageNavigationMode.Previous:
                case PageNavigationMode.First:
                    return currentPage > 0;
                case PageNavigationMode.Next:
                case PageNavigationMode.Last:
                    return currentPage + 1 < pageCount;
                default:
                    return false;
            }
        }

        private void OnRecreateDatabaseCommand()
        {
            if (MessageBox.Show($"Are you sure to recreate database?", "Confirm database recreate", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            // Release all connections
            databaseContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                $"ALTER DATABASE [{databaseContext.Database.Connection.Database}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");

            databaseContext.Database.Delete();

            // Trigger database creation
            databaseContext.Database.ExecuteSqlCommand("PRINT 'Database is created'");

            Update();
            MessageBox.Show("Database has been recreated");
        }

        private void OnSwitchPageCommand(PageNavigation pageNavigation)
        {
            switch (pageNavigation.Mode)
            {
                case PageNavigationMode.First:
                    currentPage = 0;
                    break;
                case PageNavigationMode.Last:
                    currentPage = pageCount - 1;
                    break;
                case PageNavigationMode.Next:
                    currentPage++;
                    break;
                case PageNavigationMode.Previous:
                    currentPage--;
                    break;
            }

            Update();
        }

        private void OnCategoriesCommand()
        {
            var context = new CategoriesWindowContext();
            new CategoriesWindow(context).ShowDialog();

            if (context.IsConfirmed)
            {
                Update();
            }
        }

        private void OnImportCommand()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Image (*.txt)|*.txt|All files (*.*)|*.*",
                Multiselect = true
            };

            if (dialog.ShowDialog() != true)
                return;

            int imported = 0;
            foreach (var item in dialog.FileNames)
            {
                DateTime dateTimeAdded = DateTime.Now;
                List<string> phrases  = PhraseHelper.Import(item);

                foreach (string phrase in phrases)
                {
                    if (databaseContext.Phrase.Local.Any(p => p.Content == phrase || p.PhraseRu == phrase)
                        || databaseContext.Phrase.Any(p => p.Content == phrase || p.PhraseRu == phrase))
                        continue;

                    bool isEnglish = PhraseHelper.IsEnglish(phrase);
                    databaseContext.Phrase.Add(new Phrase
                    {
                        Content = isEnglish ? phrase : null,
                        PhraseRu = isEnglish ? null : phrase,
                        DateTimeAdded = dateTimeAdded
                    });

                    imported++;
                }
            }

            databaseContext.SaveChanges();
            Update();
            MessageBox.Show($"Successfully imported {imported} phrases from {dialog.FileNames.Length} files.");
        }

        private void OnSearchCommand()
        {
            Update();
        }
    }
}
