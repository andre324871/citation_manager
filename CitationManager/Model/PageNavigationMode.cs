﻿namespace CitationManager.Model
{
    public enum PageNavigationMode
    {
        First,

        Previous,

        Next,

        Last
    }
}
