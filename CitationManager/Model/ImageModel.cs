﻿using Prism.Mvvm;
using System.Windows.Media.Imaging;

namespace CitationManager.Model
{
    public class ImageModel : BindableBase
    {
        private BitmapImage image100;
        private BitmapImage image800;

        public int? Id { get; set; }

        public byte[] Image100Bytes { get; set; }

        public byte[] Image800Bytes { get; set; }

        public BitmapImage Image100
        {
            get => image100;
            set 
            {
                image100 = value;
                RaisePropertyChanged(nameof(Image100));
            }
        }

        public BitmapImage Image800
        {
            get => image800;
            set
            {
                image800 = value;
                RaisePropertyChanged(nameof(Image800));
            }
        }
    }
}
