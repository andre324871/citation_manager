﻿using System;

namespace CitationManager.Model
{
    public class PhraseModel
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public string ContentRU { get; set; }

        public string Categories { get; set; }

        public DateTime DateTimeAdded { get; set; }

        public bool ContainsImage { get; set; }

        public bool IsImportant { get; set; }
    }
}
