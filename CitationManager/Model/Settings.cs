﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;

namespace CitationManager.Model
{
    public class Settings
    {
        private static readonly Lazy<Settings> instance = new Lazy<Settings>(Get);

        public int BigFrameWidth { get; set; }

        public int BigFrameHeight { get; set; }

        public string BigFrameString => $"{BigFrameWidth}x{BigFrameHeight}";

        public int BigFrameBorderWidthInPixels { get; set; }

        public int SmallFrameWidth { get; set; }

        public int SmallFrameHeight { get; set; }

        public string SmallFrameString => $"{SmallFrameWidth}x{SmallFrameHeight}";

        public int SmallFrameBorderWidthInPixels { get; set; }

        public int ZoomStepInPercents { get; set; }

        public static Settings Current => instance.Value;

        private static Settings Get()
        {
            try
            {
                NameValueCollection app = ConfigurationManager.AppSettings;

                int[] bigFrame = app["bigFrame"].Split('x').Select(p => int.Parse(p)).ToArray();
                int[] smallFrame = app["smallFrame"].Split('x').Select(p => int.Parse(p)).ToArray();

                return new Settings
                {
                    BigFrameWidth = bigFrame[0],
                    BigFrameHeight = bigFrame[1],
                    BigFrameBorderWidthInPixels = int.Parse(app["bigFrameBorderWidthInPixels"]),
                    SmallFrameWidth = smallFrame[0],
                    SmallFrameHeight = smallFrame[1],
                    SmallFrameBorderWidthInPixels = int.Parse(app["smallFrameBorderWidthInPixels"]),
                    ZoomStepInPercents = int.Parse(app["zoomStepInPercents"])
                };

            }
            catch (Exception exception)
            {
                throw new InvalidDataException("Could not parse settings", exception);
            }
        }
    }
}
