﻿namespace CitationManager.Model
{
    public class PageNavigation
    {
        public static readonly PageNavigation First = new PageNavigation(PageNavigationMode.First);
        public static readonly PageNavigation Previous = new PageNavigation(PageNavigationMode.Previous);
        public static readonly PageNavigation Next = new PageNavigation(PageNavigationMode.Next);
        public static readonly PageNavigation Last = new PageNavigation(PageNavigationMode.Last);

        public PageNavigation(PageNavigationMode mode)
        {
            Mode = mode;
        }

        public PageNavigationMode Mode { get; }
    }
}
