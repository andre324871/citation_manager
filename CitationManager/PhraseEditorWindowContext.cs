﻿using System;

namespace CitationManager
{
    public class PhraseEditorWindowContext
    {
        public delegate int? GetPhraseDelegate(int relativePhraseId);

        public int? PhraseId { get; set; }

        public bool IsCreation => !PhraseId.HasValue;

        /// <summary>
        /// Indicates whether dialog is confirmed. False if dialog was cancelled. Output parameter
        /// </summary>
        public bool IsConfirmed { get; set; }

        /// <summary>
        /// Action for view model to close window. Is automatically set by the window
        /// </summary>
        public Action CloseWindow { get; set; }

        /// <summary>
        /// Gets next phrase id according to current search state in main window. Returns null if this is the last phrase.
        /// </summary>
        public GetPhraseDelegate GetNextPhraseTo { get; set; }

        /// <summary>
        /// Gets previous phrase id according to current search state in main window. Returns null if this is the first phrase.
        /// </summary>
        public GetPhraseDelegate GetPreviousPhraseTo { get; set; }
    }
}
