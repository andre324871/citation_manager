﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CitationManager.Extensions
{
    public class AddConstantConverter : IValueConverter
    {
        public int Constant { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => (int)value + Constant;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
           => null;
    }
}
