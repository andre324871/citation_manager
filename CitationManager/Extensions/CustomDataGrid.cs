﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace CitationManager.Extensions
{
    /// <summary>
    /// Grid with basic support of SelectedItems binding (only from grid to source)
    /// </summary>
    public class CustomDataGrid : DataGrid
    {
        public static readonly DependencyProperty SelectedItemsListProperty =
                DependencyProperty.Register(nameof(SelectedItemsList), typeof(IList), typeof(CustomDataGrid), new PropertyMetadata(null));

        public CustomDataGrid()
        {
            SelectionChanged += OnSelectionChanged;
        }

        public IList SelectedItemsList
        {
            get => (IList)GetValue(SelectedItemsListProperty); 
            set => SetValue(SelectedItemsListProperty, value); 
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
            => SelectedItemsList = SelectedItems;
    }
}
