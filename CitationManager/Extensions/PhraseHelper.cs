﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CitationManager.Extensions
{
    public static class PhraseHelper
    {
        public static List<string> Import(string path)
        {
            var text = File.ReadAllText(path, Encoding.UTF8);

            var mixedArticleText = Regex.Replace(text, "[\\s\\r\\n]+", " ");
            mixedArticleText = mixedArticleText.Trim();

            var i = 0;
            var phrases = Regex.Split(mixedArticleText, @"(\.\)|\.""|\.|\?\)|\?""|\?|\!\)|\!""|\!)");
            var block = string.Empty;
            var delimPhrases = new List<string>();

            foreach (var phrase in phrases)
            {
                if (i > 0 && i % 2 == 0)
                {
                    delimPhrases.Add(block);
                    block = string.Empty;
                }

                if (i == 0 && phrases.Length == 1)
                {
                    delimPhrases.Add(phrase.Trim());
                    break;
                }

                block += phrase.Trim();
                i++;
            }

            return delimPhrases.Select(p => p.TrimEnd('.')).ToList();
        }

        public static bool IsEnglish(string phrase)
        {
            var coloredPhrases = new List<string>();

            var reg = new Regex("[a-z|A-Z]");
            if (reg.Match(phrase).Success)
                return true;

            reg = new Regex("[а-я|А-Я]");
            if (reg.Match(phrase).Success)
                return false;

            // English by default
            return true;
        }
    }
}
