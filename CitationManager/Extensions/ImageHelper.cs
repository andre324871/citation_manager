﻿using ImageProcessor;
using ImageProcessor.Imaging;
using ImageProcessor.Imaging.Formats;
using MetadataExtractor;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;

namespace CitationManager.Extensions
{
    public static class ImageHelper
    {
        public static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0)
                return null;

            var image = new BitmapImage();

            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;

                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }

            image.Freeze();

            return image;
        }

        public static BitmapImage LoadImage(MemoryStream memory)
        {
            memory.Position = 0;
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memory;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();

            return bitmapImage;
        }

        public static byte[] ToByteArray(BitmapImage imageSource)
        {
            var encoder = new JpegBitmapEncoder
            {
                QualityLevel = 100
            };

            encoder.Frames.Add(BitmapFrame.Create(imageSource));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                return ms.ToArray();
            }
        }

        public static byte[] ToByteArray(Image image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Jpeg);

            return ms.ToArray();
        }

        public static byte[] LoadImageFromFile(string fileName)
        {
            using (Stream stream = File.Open(fileName, FileMode.Open))
            {
                byte[] photoBytes = ToByteArray(Image.FromStream(stream));

                using (var inStream = new MemoryStream(photoBytes))
                using (var outStream = new MemoryStream())
                using (var imageFactory = new ImageFactory(preserveExifData: false))
                {
                    var directories = ImageMetadataReader.ReadMetadata(inStream);

                    int rotateDegrees = 0;
                    if (directories.Any(d => d.Tags.Any(t => t.Name == "Orientation" && t.Description?.Contains("(Rotate 90 CW)") == true)))
                        rotateDegrees = 90;

                    inStream.Position = 0;

                    imageFactory.Load(inStream)
                                .Rotate(rotateDegrees)
                                .Quality(100)
                                .Format(new JpegFormat { Quality = 100 })
                                .BackgroundColor(Color.White)
                                .Save(outStream);

                    outStream.Position = 0;

                    return outStream.GetBuffer();
                }
            }
        }

        public static BitmapImage BitmapToBitmapImage(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Jpeg);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

        public static byte[] Resize(byte[] photoBytes, int width, int height)
        {
            using (var inStream = new MemoryStream(photoBytes))
            using (var outStream = new MemoryStream())
            using (var imageFactory = new ImageFactory(preserveExifData: true))
            {
                imageFactory.Load(inStream)
                            .Resize(new ResizeLayer(new Size(width, height), ResizeMode.Max))
                            .Quality(100)
                            .Format(new JpegFormat { Quality = 100 })
                            .Save(outStream);

                outStream.Position = 0;

                return outStream.GetBuffer();
            }
        }

        public static byte[] ResizeWithCrop(byte[] photoBytes, int canvasWidth, int maxHeight)
        {
            ISupportedImageFormat format = new JpegFormat { Quality = 100 };
            var size = new Size(canvasWidth, 0);

            using (var inStream = new MemoryStream(photoBytes))
            using (var outStream = new MemoryStream())
            using (var imageFactory = new ImageFactory(preserveExifData: true))
            {
                imageFactory.Load(inStream)
                            .Resolution(300, 300)
                            .Resize(size)
                            .Crop(new Rectangle(0, 0, canvasWidth, maxHeight))
                            .Quality(100)
                            .Format(format)
                            .BackgroundColor(Color.White)
                            .Save(outStream);

                outStream.Position = 0;

                return outStream.GetBuffer();
            }
        }

        public static byte[] ResizeWithPad(byte[] photoBytes, int maxWidth)
        {
            ISupportedImageFormat format = new JpegFormat { Quality = 100 };
            var size = new Size(maxWidth, maxWidth);

            using (var inStream = new MemoryStream(photoBytes))
            using (var outStream = new MemoryStream())
            using (var imageFactory = new ImageFactory(preserveExifData: true))
            {
                imageFactory.Load(inStream)
                            .Resolution(300, 300)
                            .Resize(size)
                            .Quality(100)
                            .Format(format)
                            .BackgroundColor(Color.White)
                            .Save(outStream);

                outStream.Position = 0;

                return outStream.GetBuffer();
            }
        }
    }
}
