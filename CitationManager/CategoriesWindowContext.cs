﻿using System;

namespace CitationManager
{
    public class CategoriesWindowContext
    {
        /// <summary>
        /// Indicates whether dialog is confirmed. False if dialog was cancelled. Output parameter
        /// </summary>
        public bool IsConfirmed { get; set; }

        /// <summary>
        /// Action for view model to close window. Is automatically set by the window
        /// </summary>
        public Action CloseWindow { get; set; }
    }
}
