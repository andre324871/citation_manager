﻿using System.Windows.Input;

namespace CitationManager
{
    public partial class CategoriesWindow
    {
        public CategoriesWindow(CategoriesWindowContext context)
        {
            InitializeComponent();

            var viewModel = (CategoriesWindowViewModel)DataContext;
            context.CloseWindow = Close;
            viewModel.Context = context;
            PreviewKeyDown += OnPreviewKeyDown;
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Escape)
                return;

            Close();
            e.Handled = true;
        }
    }
}
