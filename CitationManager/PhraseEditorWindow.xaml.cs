﻿using System.Windows.Input;

namespace CitationManager
{
    public partial class PhraseEditorWindow 
    {
        private PhraseEditorWindowViewModel viewModel;

        public PhraseEditorWindow(PhraseEditorWindowContext context)
        {
            InitializeComponent();

            viewModel = (PhraseEditorWindowViewModel)DataContext;
            context.CloseWindow = Close;
            viewModel.Context = context;
            viewModel.OnInitialized();
            PreviewKeyDown += OnPreviewKeyDown;
        }

        private void OnAssignedMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (viewModel.UnassignCommand.CanExecute())
            {
                viewModel.UnassignCommand.Execute();
            }
        }

        private void OnAvailableMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (viewModel.AssignCommand.CanExecute())
            {
                viewModel.AssignCommand.Execute();
            }
        }

        private void OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Escape)
                return;

            Close();
            e.Handled = true;
        }
    }
}
