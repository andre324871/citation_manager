﻿using CitationManager.DAL;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;

namespace CitationManager
{
    public class CategoriesWindowViewModel : BindableBase
    {
        private List<Category> categories;
        private List<Category> initialCategories;
        private Entities databaseContext = new Entities();

        public CategoriesWindowViewModel()
        {
            Categories = databaseContext.Category.ToList();
            initialCategories = Categories.ToList();
            ApplyCommand = new DelegateCommand(OnApplyCommand);
        }

        public DelegateCommand ApplyCommand { get; }

        public CategoriesWindowContext Context { get; set; }

        public List<Category> Categories
        {
            get => categories;
            set
            {
                categories = value;
                RaisePropertyChanged(nameof(Categories));
            }
        }

        private void OnApplyCommand()
        {
            databaseContext.Category.AddRange(Categories.Where(c => !initialCategories.Contains(c)));

            foreach (var category in initialCategories.Where(c => !Categories.Contains(c)))
            {
                databaseContext.Category.Remove(category);
            }

            databaseContext.SaveChanges();
            Context.IsConfirmed = true;
            Context.CloseWindow();
        }
    }
}
